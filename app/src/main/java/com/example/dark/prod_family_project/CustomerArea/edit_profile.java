package com.example.dark.prod_family_project.CustomerArea;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.dark.prod_family_project.Activities.Login;
import com.example.dark.prod_family_project.R;
import com.example.dark.prod_family_project.SessionManager.SessionManager;
import com.example.dark.prod_family_project.Singletons.RequestQueues;
import com.example.dark.prod_family_project.VolleyRequests.EditProdile;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;


public class edit_profile extends Fragment {

    private static final String TAG="edit_profile";

    private TextInputLayout name,email,password,old_password;
    private Button update;

    FirebaseStorage storage;
    StorageReference storageReference;

    Bitmap profilePicture;

    FloatingActionButton logout;

    ImageView imageView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_profile,container,false);
        initialize(view);
        getcredentials(view);
        LOGOUT();
        return view;
    }

    private void LOGOUT() {

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Logged out.", Toast.LENGTH_SHORT).show();
                new SessionManager(getContext()).Logout();
                getContext().startActivity(new Intent(getContext(),Login.class));
            }
        });
    }

    private void initialize(View view) {

        name = (TextInputLayout)view.findViewById(R.id.edit_name);
        email = (TextInputLayout)view.findViewById(R.id.edit_email);
        password = (TextInputLayout)view.findViewById(R.id.edit_password);
        old_password = (TextInputLayout)view.findViewById(R.id.old_password);

        update = (Button)view.findViewById(R.id.update_prod);

        imageView = (ImageView)view.findViewById(R.id.edit_image);

        logout = (FloatingActionButton)view.findViewById(R.id.logout_edit_prof);
    }


    private void getcredentials(View view) {

        setcredentials();

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {





                final ProgressDialog progressDialog = new ProgressDialog(getContext());
                progressDialog.setTitle("Please Wait");
                progressDialog.setMessage("Updating profile");
                progressDialog.setCancelable(false);
                progressDialog.show();
                EditProdile request = new EditProdile(new SessionManager(getContext()).getId(),name.getEditText().getText().toString(),
                        email.getEditText().getText().toString()
                        , old_password.getEditText().getText().toString(),
                        password.getEditText().getText().toString(), new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {


                            JSONObject jsonObject = new JSONObject(response);
                            if(jsonObject.getBoolean("PASSWORD")){
                                progressDialog.dismiss();
                                Toast.makeText(getContext(), "Incorrect current password.", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            progressDialog.dismiss();
                            Toast.makeText(getContext(), "Profile Updated", Toast.LENGTH_SHORT).show();


                        }catch (Exception e){

                            Log.i("Exception", e.getMessage().toString());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Volley Exception", error.getMessage().toString());
                    }
                });
                RequestQueues.getInstance(getContext()).addToRequestQue(request);

            }
        });





    }

    private void getimage() {
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        final String url = new SessionManager(getContext()).getImage();

        Picasso.with(getContext()).load(url).into(imageView);


    }
    private void setcredentials() {


        try{


            name.getEditText().setText(new SessionManager(getContext()).getName());
            email.getEditText().setText(new SessionManager(getContext()).getEmail());

            getimage();
            //Picasso.with(getContext()).load(new SessionManager(getContext()).getImage()).into(imageView);


        }catch(Exception e){
            Log.d("Exception",""+e.getMessage().toString());
            Toast.makeText(getContext(), ""+e.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }
}
