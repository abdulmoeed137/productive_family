package com.example.dark.prod_family_project.Activities;

import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.example.dark.prod_family_project.Adapters.TabAdapter;
import com.example.dark.prod_family_project.AdminArea.Admin_fragment2;
import com.example.dark.prod_family_project.AdminArea.Admin_fragments;
import com.example.dark.prod_family_project.CustomerArea.edit_profile;
import com.example.dark.prod_family_project.CustomerArea.history;
import com.example.dark.prod_family_project.CustomerArea.products;
import com.example.dark.prod_family_project.R;

public class Customer_Dashboard extends AppCompatActivity {


    private TabAdapter tabAdapter;

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer__dashboard);

        tabAdapter = new TabAdapter(getSupportFragmentManager());

        viewPager =(ViewPager)findViewById(R.id.container);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout)findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager){

        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addfragment(new products(),"products");
        adapter.addfragment(new edit_profile(),"edit_profile");
        adapter.addfragment(new history(),"history");
        viewPager.setAdapter(adapter);

            }
    }

