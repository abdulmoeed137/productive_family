package com.example.dark.prod_family_project.CustomerArea;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.example.dark.prod_family_project.R;

public class BillPayment extends AppCompatActivity {

    int bill;

    private Button paypal,cash;
    TextView amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_payment);
        initialize();
        function();

    }

    private void function() {

        bill = getIntent().getIntExtra("Bill",0);

        amount.setText(String.valueOf(bill));

    }

    private void initialize() {

        paypal = (Button)findViewById(R.id.paypal);
        cash = (Button)findViewById(R.id.cash);

        amount = (TextView)findViewById(R.id.amount);

    }



}
