package com.example.dark.prod_family_project.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.dark.prod_family_project.Adapters.categoriesAdapter;
import com.example.dark.prod_family_project.Holder.all_holders;
import com.example.dark.prod_family_project.Models.all_categories;
import com.example.dark.prod_family_project.Product_family.add_product;
import com.example.dark.prod_family_project.Product_family.edit_product;
import com.example.dark.prod_family_project.R;
import com.example.dark.prod_family_project.SessionManager.SessionManager;
import com.example.dark.prod_family_project.Singletons.RequestQueues;
import com.example.dark.prod_family_project.VolleyRequests.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Prod_Family extends AppCompatActivity {

    ArrayList<all_categories> fetched = new ArrayList<>();
    categoriesAdapter arrayAdapter;
    all_categories model_categories;

    Context ctx;

    private Button add_products,my_products,orders;
    private ImageView settings,logout;
    Intent intent;


    add_product add_product;

    all_holders holder;

    private TextView name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prod__family);
        initialize();
        intro();
    }


    private void intro() {

        name.setText(new SessionManager(Prod_Family.this).getName());

        add_products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(Prod_Family.this,seller_frame_for_fragment.class);
                intent.putExtra("value","add");
                startActivity(intent);
                finish();

            }
        });
        my_products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(Prod_Family.this,seller_frame_for_fragment.class);
                intent.putExtra("value","edit");
                startActivity(intent);
                finish();
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SessionManager(Prod_Family.this).Logout();
                finish();
                startActivity(new Intent(Prod_Family.this,Login.class));

            }
        });
        orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(Prod_Family.this,Orders.class));
            }
        });
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(Prod_Family.this,seller_frame_for_fragment.class);
                intent.putExtra("value","settings");
                startActivity(intent);
                finish();
            }
        });

    }


    private void initialize() {

        add_products = (Button)findViewById(R.id.add_products);
        my_products = (Button)findViewById(R.id.my_products);
        orders = (Button)findViewById(R.id.Orders);
        logout = (ImageView)findViewById(R.id.logout_seller);

        settings = (ImageView)findViewById(R.id.SETTINGS);

        name = (TextView)findViewById(R.id.seller_name);
    }
}
