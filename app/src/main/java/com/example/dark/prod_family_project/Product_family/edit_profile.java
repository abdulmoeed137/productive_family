package com.example.dark.prod_family_project.Product_family;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.dark.prod_family_project.R;

/**
 * Created by abd on 02-Feb-18.
 */

public class edit_profile extends Fragment{


    ImageView seller_img;

    TextInputLayout name,email,old,new_pass;

    Button update;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.seller_edit_profile,null,false);
        return view;
    }
}
